public class StudentRecordExample {
	public static void main (String[] args) {
		//membuat 3 object StudentRecord
		StudentRecord annaRecord = new StudentRecord();
		StudentRecord beahRecord = new StudentRecord();
		StudentRecord crisRecord = new StudentRecord();
		StudentRecord karyonoRecord = new StudentRecord();
		StudentRecord songjongkiRecord = new StudentRecord();
		StudentRecord masbejoRecord = new StudentRecord();

		//memberi nama siswa
		annaRecord.setName("Anna");
		beahRecord.setName("Beah");
		crisRecord.setName("Cris");
		karyonoRecord.setName("Karyono");
		songjongkiRecord.setName("Song Jong-Ki");
		masbejoRecord.setName("Mas Bejo");
		
		//Menampilkan nama siswa
		System.out.println( annaRecord.getName() );
		System.out.println( beahRecord.getName() );
		System.out.println( crisRecord.getName() );
		System.out.println( karyonoRecord.getName() );
		System.out.println( songjongkiRecord.getName() );
		System.out.println( masbejoRecord.getName() );
		
		System.out.println();
		//Menampilkan jumlah siswa
		System.out.println("Jumlah = "+ StudentRecord.getStudentCount() +" orang");
		StudentRecord anna2Record = new StudentRecord();
		anna2Record.setName("Anna");
		anna2Record.setAddress("Philipina");
		anna2Record.setAge(15);
		anna2Record.setMathGrade(80);
		anna2Record.setEnglishGrade(90.5);
		anna2Record.setScienceGrade(100);
		
		//overload method
		//anna2Record.print(anna2Record.getName());
		//anna2Record.print(anna2Record.getEnglishGrade());
		//anna2Record.getMathGrade();
		//anna2Record.getScienceGrade();
		//annaRecord.print(annaRecord.getName());
	}
}