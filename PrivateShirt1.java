public class PrivateShirt1 {
	private int idBaju = 0;
	private String keterangan = "-Keterangan Diperlukan-";
	//Kode warna R=Merah, G=Hijau, B=Biru, U=Tidak Ditentukan
	private char kodeWarna = 'U';
	private double harga = 0.0;
	private int jmlStok = 0;
	
	public char getKodeWarna() {
		return kodeWarna;
	}
	
	public void setKodeWarna(char kode) {
		kodeWarna = kode;
	}
}