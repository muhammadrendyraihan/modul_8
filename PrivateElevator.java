public class PrivateElevator {
	private boolean bukaPintu = false;
	private int lantaiSkrg = 1;
	private int berat = 0;
	
	private final int Kapasitas = 1000;
	private final int Lantai_Atas = 5;
	private final int Lantai_Bawah = 1;
	
	public void setbukaPintu(boolean temp) {
		this.bukaPintu = temp;
	}
	
	public boolean getbukaPintu() {
		return bukaPintu;
	}
	
	public void setlantaiSkrg(int temp) {
		this.lantaiSkrg = temp;
	}
	
	public int getlantaiSkrg() {
		return lantaiSkrg;
	}
}